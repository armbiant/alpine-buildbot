const config = require('../node_modules/common-sk/jsdoc.common.js');

config.source.include = ['../node_modules/elements-sk', '../node_modules/elements-sk/styles', '../node_modules/common-sk/modules', './jsdoc/README.md'];
module.exports = config;
